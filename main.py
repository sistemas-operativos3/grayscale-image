from flask import Flask, request, jsonify
from flask_cors import CORS

import cv2
import numpy as np

import base64

import qimage2ndarray as q2n
from PyQt5 import QtCore

import io
from PIL import Image

app = Flask(__name__)
app.config['DEBUG'] = True
CORS(app)


@app.route('/grayScale', methods=['POST'])
def grayScale():

    base64_decoded = base64.b64decode(request.form.get('data'))

    image = Image.open(io.BytesIO(base64_decoded))
    image_np = np.array(image)

    # cv2.imshow('original', image_np)

    gray = cv2.cvtColor(image_np, cv2.COLOR_BGR2GRAY)

    # cv2.imshow('gray', gray)

    buffer = QtCore.QBuffer()
    buffer.open(buffer.WriteOnly)
    buffer.seek(0)

    gray = q2n.array2qimage(gray)

    gray.save(buffer, "PNG", quality=100)
    encoded = bytes(buffer.data().toBase64()).decode()

    return jsonify({
        'data': encoded
    }), 200
    # cv2.waitKey(0)
    # cv2.destroyAllWindows()


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5001)